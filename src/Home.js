import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import Logo from './assets/ecosurf_logo.png';
import './styles.css'

function Home() {
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem('connected') === "true")
      navigate("/dashboard")
  }, [])

  function handleClick() {
    localStorage.setItem('connected', true);
    navigate("/dashboard");
  }

  return (
    <div className="splitScreen gradient-form">
      <div className="halfPane items-col-center">
        <img src={Logo}
          style={{ width: '185px' }} alt="logo" />
        <form>
          <input type="email"
            className="form-control mb-4"
            placeholder="Adresse mail"
            onChange={(e) => setEmail(e.target.value)} />

          <input type="password"
            className="form-control mb-4"
            placeholder="Mot de passe"
            onChange={(e) => setPassword(e.target.value)} />

          <button type="submit" className="btn btn-primary gradient-custom-2 mb-4 w-100" onClick={handleClick}>Connexion</button>
          <p>
            <a className="text-muted" href="#">Mot de passe oublié ?</a>
          </p>
        </form>
        <div className="pb-4 mb-4">
          <p>Vous n'avez pas de compte ? <button className="btn btn-default button-create-account">Créer un compte</button></p>
        </div>
      </div>

      <div className="halfPane explanationPane gradient-custom-2 items-col-center px-3 py-4">
        <h4>Tableau de bord EcoSurf</h4>
        <p>Ceci est un tableau de bord d'exemple permettant aux clients d'un FAI de visualiser leur consommation de CO₂ et d'analyser leur activité pour adapter leur usage d'Internet.
        </p>
      </div>
    </div>
  );
}

export default Home;