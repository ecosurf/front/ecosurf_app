import React, { useState, useEffect } from "react";

import UserManager from "./services/UserManager";
import ProgressBar from 'react-bootstrap/ProgressBar';
import CanvasJSReact from './assets/canvasjs.react';
import './styles.css'

const userManager = new UserManager();
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function Profile() {

    const [percent, setPercent] = useState(0);
    const [optionsCircularGraph, setOptionsCircularGraph] = useState({});
    const [optionsColumnGraph, setOptionsColumnGraph] = useState({});

    useEffect(() => {
        getCurrentData();
        getCurrentDataByCategories();
        getHistorical();
    }, []);

    const getCurrentData = async () => {
        const res = await userManager.getCurrentData();
        setPercent(res.totalQuantityCO2 / res.User.Thresold.value * 100);
    }

    const getCurrentDataByCategories = async () => {
        const res = await userManager.getCurrentDataByCategories();

        const totalQuantityCO2 = res.reduce(
            (accumulator, data) => accumulator + parseInt(data.totalQuantityCO2),
            0
        );
        const data = res.map((data) => { return { name: data.Category.name, y: parseInt(data.totalQuantityCO2) / totalQuantityCO2 * 100 } })

        setOptionsCircularGraph({
            animationEnabled: true,
            title: {
                text: "Consommation CO₂ par catégorie",
                fontFamily: "Open Sans",
                fontSize: 20
            },
            subtitles: [],
            data: [{
                type: "doughnut",
                showInLegend: true,
                indexLabel: "{name}: {y}",
                yValueFormatString: "#,###'%'",
                dataPoints: data
            }]
        })
    }

    const getHistorical = async () => {
        const res = await userManager.getHistorical();
        const months = { 1: 'Janvier', 2: 'Février', 3: 'Mars', 4: 'Avril', 5: 'Mai', 6: 'Juin', 7: 'Juillet', 8: 'Août', 9: 'Septembre', 10: 'Octobre', 11: 'Novembre', 12: 'Décembre' }
        const data = res.map((data) => { return { label: months[data.month], y: parseInt(data.totalQuantityCO2), color: "green" } })

        setOptionsColumnGraph({
            title: {
                text: "Relevé des 5 derniers mois",
                fontFamily: "Open Sans",
                fontSize: 20
            },
            data: [
                {
                    type: "column",
                    dataPoints: data
                }
            ]
        })
    }

    return (
        <section className="my-2">
            <div className="container">
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <p className="text-center my-2 fw-semibold">Consommation de CO₂ sur le mois</p>
                        <ProgressBar animated centered now={percent} label={`${percent}%`} variant="danger" min={0} max={100} />
                    </div>
                </div>
            </div>

            <div className="container">
                <div class="row justify-content-md-center my-5">
                    <div class="col-6">
                        <CanvasJSChart options={optionsCircularGraph} />
                    </div>
                    <div class="col-6 p-0">
                        <CanvasJSChart options={optionsColumnGraph} />
                    </div>
                </div>
            </div>

        </section>

    );
}

export default Profile;