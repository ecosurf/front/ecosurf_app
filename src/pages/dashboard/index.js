import React, { useState, useEffect } from "react";

import Header from '../../components/header'
import ProgressBar from 'react-bootstrap/ProgressBar';
import UserManager from "../../services/UserManager";
import CanvasJSReact from '../../assets/canvasjs.react';

const userManager = new UserManager();
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const Dashboard = () => {

    const [percent, setPercent] = useState(0);
    const [colorPercent, setColorPercent] = useState("green");

    const [threshold, setThreshold] = useState(0);
    const [totalQuantityCO2, setTotalQuantityCO2] = useState(0);

    const [optionsCircularGraph, setOptionsCircularGraph] = useState({});
    const [optionsColumnGraph, setOptionsColumnGraph] = useState({});

    useEffect(() => {
        getCurrentData();
        getCurrentDataByCategories();
        getHistorical();
    }, []);


    const defineColorProgressBar = (percent) => {
        if (percent <= 0.5)
            return 'success';
        else if (percent > 0.5 && percent <= 0.8)
            return 'warning'
        else return 'danger';
    }

    const getCurrentData = async () => {
        const res = await userManager.getCurrentData();
        setPercent((res.totalQuantityCO2.toFixed(2) / res.User.Thresold.value * 100).toFixed(2));
        setThreshold(res.User.Thresold.value)
        setTotalQuantityCO2(res.totalQuantityCO2.toFixed(2))
        setColorPercent(defineColorProgressBar(res.totalQuantityCO2.toFixed(2) / res.User.Thresold.value))
    }

    const defineColorChart = (percent) => {
        if (percent <= 0.5)
            return 'rgb(25, 135, 84)';
        else if (percent > 0.5 && percent <= 0.8)
            return 'rgb(255, 193, 7)'
        else return 'rgb(220, 53, 69)';
    }

    const getCurrentDataByCategories = async () => {
        const res = await userManager.getCurrentDataByCategories();

        const totalQuantityCO2 = res.reduce(
            (accumulator, data) => accumulator + parseFloat(data.totalQuantityCO2.toFixed(2)),
            0.0
        );

        const data = res.map((data) => { return { name: data.Category.name, y: data.totalQuantityCO2.toFixed(2) / totalQuantityCO2 * 100 } })

        setOptionsCircularGraph({
            animationEnabled: true,
            title: {
                text: "Consommation CO₂ par catégorie",
                fontFamily: "Open Sans",
                fontSize: 16,
                fontColor: "#403e56",
                fontWeight: "bold"
            },
            height: 300,
            data: [{
                type: "doughnut",
                showInLegend: true,
                indexLabel: "{name}: {y}",
                yValueFormatString: "#,###'%'",
                dataPoints: data
            }]
        })
    }

    const getHistorical = async () => {
        const res = await userManager.getHistorical();
        const months = { 1: 'Janvier', 2: 'Février', 3: 'Mars', 4: 'Avril', 5: 'Mai', 6: 'Juin', 7: 'Juillet', 8: 'Août', 9: 'Septembre', 10: 'Octobre', 11: 'Novembre', 12: 'Décembre' }
        const data = res.map((data) => { return { label: months[data.month], y: parseFloat(data.totalQuantityCO2.toFixed(2)), color: defineColorChart(data.totalQuantityCO2.toFixed(2) / data.User.Thresold.value) } })

        setOptionsColumnGraph({
            title: {
                text: "Relevé des 5 derniers mois",
                fontFamily: "Open Sans",
                fontSize: 16,
                fontColor: "#403e56",
                fontWeight: "bold"
            },
            height: 300,
            axisY: {
                gridThickness: 0,
            },
            data: [
                {
                    type: "column",
                    dataPoints: data
                }
            ]
        })
    }

    return (
        <section className='sm:flex'>
            <Header />
            <section className='my-10 max-w-5xl w-full px-4'>
                <h3 className='font-semibold text-3xl'>Tableau de bord</h3>
                <p className='font-light'>Rebonjour, Pierre</p>

                <div className='flex flex-col items-start sm:flex-row sm:items-center sm:justify-around'>

                    <div className='flex justify-center items-center my-2'>
                        <div className='flex justify-center items-center rounded-lg bg-gray-200 w-10 h-10'>
                            <img className="h-6 w-6 primary-text-color" src={`${process.env.PUBLIC_URL + "/jauge-icon.png"}`} alt="icon-jauge" />
                        </div>
                        <div className='mx-3'>
                            <p className="font-semibold m-0 text-sm secondary-text-color">Consommation totale pour ce mois</p>
                            <ProgressBar className="my-2" centered now={percent} label={`${percent}%`} variant={colorPercent} min={0} max={100} />
                        </div>
                    </div>

                    <div className='flex justify-center items-center my-2'>
                        <div className='flex justify-center items-center rounded-lg bg-gray-200 w-10 h-10'>
                            <img className="h-4 w-6 primary-text-color" src={`${process.env.PUBLIC_URL + "/co2-icon.png"}`} alt="icon-co2" />
                        </div>
                        <div className='mx-3'>
                            <p className='font-semibold m-0 text-sm secondary-text-color '>Votre consommation de CO₂</p>
                            <p className='font-bold m-0 text-2xl'>{totalQuantityCO2} kg</p>
                        </div>
                    </div>
                    <div className='flex justify-center items-center my-2'>
                        <div className='flex justify-center items-center rounded-lg bg-gray-200 w-10 h-10'>
                            <img className="h-4 w-6 primary-text-color" src={`${process.env.PUBLIC_URL + "/up-icon.png"}`} alt="icon-co2" />
                        </div>
                        <div className='mx-3'>
                            <p className='font-semibold m-0 text-sm secondary-text-color '>Seuil maximal de CO₂</p>
                            <p className='font-bold m-0 text-2xl'>{threshold} kg</p>
                        </div>
                    </div>
                </div>

                <div className="sm:flex justify-around p-3 mt-2 sm:mt-5">
                    <div className="my-2 sm:my-0 sm:w-1/3">
                        <CanvasJSChart options={optionsCircularGraph} />
                    </div>
                    <div className="mt-20 sm:mt-0 sm:w-1/3">
                        <CanvasJSChart options={optionsColumnGraph} />
                    </div>
                </div>
            </section>
        </section>
    )
}
export default Dashboard;