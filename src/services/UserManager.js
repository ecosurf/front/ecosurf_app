import axios from 'axios';

const URL = process.env.REACT_APP_URL_USERMANAGER;
const UserId = 1;

class UserManager {

    getCurrentData = async () => {
        const res = await axios.get(`${URL}/user/current?userId=${UserId}`);
        return res.data;
    }

    getCurrentDataByCategories = async () => {
        const res = await axios.get(`${URL}/user/current/categories?userId=${UserId}`);
        return res.data;
    }

    getHistorical = async () => {
        const res = await axios.get(`${URL}/user/historical?userId=${UserId}`);
        return res.data;
    }
}

export default UserManager;