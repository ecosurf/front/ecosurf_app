import React from "react";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import Home from "./Home";
import Dashboard from './pages/dashboard';

function App() {

  return (
    <Router>
      <div>
        <Routes>
          <Route path="*" element={<Home />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;