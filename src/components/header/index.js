import './index.css'
const Header = () => {

    const actif = window.location.pathname;

    return (
        <section className="sm:w-1/5 flex sm:flex-col sm:items-center">
            <div className="sm:flex w-full sm:h-full">
                <div className='sm:flex sm:flex-col sm:items-center px-5'>
                    <div className="my-4" >
                        <a className="mx-auto" href="/">
                            <img className='w-24 h-12' alt="logo EcoSurf" src={`${process.env.PUBLIC_URL + "/logo.png"}`} />
                        </a>
                    </div>
                    <div className='sm:my-5 text-center'>
                        <img className='w-20 h-20 m-auto' src={`${process.env.PUBLIC_URL + "/user-picture.jpg"}`} alt="user-picture" />
                        <p className='font-bold'>Pierre DUPONT</p>
                    </div>
                    <div className="flex flex-col items-center">
                        <a className={`my-3 subtitle no-underline ${actif === '/dashboard' && 'link-active'}`} href="/dashboard">Tableau de bord</a>
                    </div>

                    <div className='sm:mt-32 text-center'>
                        <a className='font-light subtitle no-underline' href="/login" onClick={() => localStorage.setItem('connected', false)}>Se déconnecter</a>
                    </div>
                </div>
                <div>
                    <div className='divider my-5 hidden sm:block'></div>
                </div>
            </div>
        </section >
    )
}
export default Header;